﻿
.. include:: ../Includes.txt

============
Introduction
============

[NITSAN] Instagram
==================

.. figure:: Images/TYPO3-instagram-banner.jpg
   :alt: Extension Banner 

What does it do?
================

Plugin to display Instagram Feed with clean, customizable and responsive feeds from your Instagram account.

It’s awesome TYPO3 extension which provides you a very attractive frontend display of Instagram posts at your TYPO3 website.


Screenshots
===========

**Feed using Javascript**

.. figure:: Images/screenshot_1.jpeg
   :alt: extension screenshot1

**Feed using Instagram V1 API**

.. figure:: Images/screenshot_2.jpeg
   :alt: extension screenshot1

**Feed using Instagram Basic Display API**

.. figure:: Images/screenshot_3.jpeg
   :alt: extension screenshot1

