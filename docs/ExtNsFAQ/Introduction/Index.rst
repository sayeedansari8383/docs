﻿
.. include:: ../Includes.txt

============
Introduction
============


[NITSAN] Ultimate FAQ
=====================

.. figure:: Images/ext_banner.png
   :alt: Extension Banner 


What does it do?
================

Ultimate FAQ TYPO3 Extension is an easy-to-use and customizable extension to shape and display on your website a list of the most frequent customer questions with answers. Informative and easy-to navigate FAQ extension helps to anticipate clients' enquiries and reduce time and cost of Support team. Using this FAQ may also help share more details about your products and services, focus attention on their benefits, and and eliminate any possible misconceptions and doubts to increase your sales.


**FEATURES:**

- Dashboard (Statistics, FAQ Summary etc)

- Manage FAQs (Add, Edit, Delete FAQs)

- Category Wise FAQs (Assign Categories to FAQs)

- Set Custom CSS

- Scroll To Top 

- Manage Categories (Add, Edit, Delete system categories)

- Configure FAQ at Multiple pages

- FAQ Toggle & Accordion

- Select specific FAQs to display

- Categories Toggle & Accordion

- Enable/Disable Cateogires, Questions/Answers

- Expand/Collapse all FAQs with single click

- Sort Categories & FAQs

- Set icons for FAQ and Categories

- Icons Styling (colors, fonts, borders etc)

- Block & Border Block Themes

- FAQs Questions and Answers Styling (colors, fonts, borders etc)

- Categories Layouts and Styling

- Heading Style Options

- Animation and Effects for FAQs

- Search Feature

- Submit Site Visitors Own FAQ (Forns, Moderation, Emails etc)

- Social Media Sharing

- FAQ Moderation from Backend and Email


Screenshots
===========

Category-wise FAQs
^^^^^^^^^^^^^^^^^^

.. figure:: Images/category_wise_faqs.png
   :alt: Category-wise FAQs 

List of FAQs
^^^^^^^^^^^^

.. figure:: Images/faq_list.jpeg
   :alt: FAQ list 

Submit your FAQ Form
^^^^^^^^^^^^^^^^^^^^

.. figure:: Images/faq_form.jpeg
   :alt: Submit your FAQ Form 