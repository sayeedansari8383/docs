﻿
.. include:: ../Includes.txt

============
Introduction
============

ext:ns_statcounter
===================

.. figure:: Images/TYPO3_API_StatCounter_NITSAN_Banner_Preview.jpg
   :alt: TYPO3 EXT TYPO3_API_StatCounter_NITSAN_Banner_Preview Banner
   :width: 1000px

.. _What-does-it-do:

What does it do?
================

StatCounter is a free web traffic analysis service, which provides summary stats on all your traffic and a detailed analysis of your last 500 page views. This limit can be increased by subscribing to their paid service.

The StatCounter TYPO3 Extension brings you all the powerful StatCounter features to your TYPO3 site. `http://statcounter.com/features/ <http://statcounter.com/features/>`_

.. _Screen-shots:

Screen Shots
================


.. figure:: Images/TYPO3_StatsCounter_Backend_1.png
   :alt: screenshot "TYPO3_StatsCounter_Backend_1"
   :class: with-shadow



.. figure:: Images/TYPO3_StatsCounter_Backend_2.png
   :alt: screenshot "TYPO3_StatsCounter_Backend_2"
   :class: with-shadow
