﻿
.. include:: ../Includes.txt

============
Introduction
============

[NITSAN] HelloBar TYPO3 Extension
=================================

.. figure:: Images/TYPO3_EXT_nitsan_hellobar.jpg
   :alt: Hellobar Extension Banner
   :width: 1200px

.. _What-does-it-do:

What does it do?
================

Hello Bar is a tool for a website that gives you a chance to configure and design messages for your guests. It gives you tools and services that guarantee the correct timing of messages you need your guests to see.

- Easily configure HelloBar Panels
- Enable/Disable HelloBar Panels
- Convert visitors into Customer
- Increase potential customer list
- Display right message at the right time
- Decrease bounce Rate
- Subscribe feature
