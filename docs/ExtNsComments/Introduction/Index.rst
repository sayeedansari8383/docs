﻿
.. include:: ../Includes.txt

============
Introduction
============


[NITSAN] Comments plugin for TYPO3
==================================

.. figure:: Images/extension_banner.jpg
   :alt: Extension Banner 


What does it do?
================

NS_Comment plugin lets site visitors easily add comments on a published page of the sites. The plugin adds clean and pleasing comment UI and uses nesting comment structure to increase engagement on the pages. NS_Comment comes with Email notifications for Admins and also allows them to moderate the comments easily from Email or Backend.

**FEATURES:**

- Comment Moderation Feature for Admin

- Threaded Comments (with Nested-level Reply-to features)

- Enable/Disable Comments for fe_users (Frontend Loggedin Users)

- Get Email Notification to Admin on New Comments

- Set User (Commentator) Profile Image

- Restrict/Allow Annonymous Users to Comment

- Direct Comment Approval from Email sent to Admin

- Enable/Disable Captcha

- Custom Date & Time format

- Add "Terms" checkbox in Comment form

- Add your custom CSS/JS

- Each news have its own set comments

- Easy manipulation of comemnts


Screenshot
==========

Comment Form
^^^^^^^^^^^^

.. figure:: Images/add_comment.png
   :alt: Add Comment form 

Comment List
^^^^^^^^^^^^

.. figure:: Images/comments_list.png
   :alt: List of comments 