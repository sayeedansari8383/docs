
.. include:: ../Includes.txt

============
Introduction
============


[NITSAN] Cookies hint
=====================

.. figure:: Images/typo3-ext-cookies_hint.jpg
   :alt: Extension Banner


What does it do?
================

Do you want to add a cookies consent popup in your TYPO3 website? The European Union’s cookie law requires websites to get user consent to set any cookies on their website. As a TYPO3 website owner, your website may be using cookies as well. The Cookie Hint plugin will assist you in making your website GDPR compliant.

**FEATURES:**

- The plugin will enable a notice with Accept and Reject options. The cookies are not rendered by default and only upon user consent.

- The cookie value will be set to ‘null’ by default; it takes a value ‘yes’ when the user clicks ‘Accept’ and ‘no’ upon ‘Reject’. Your developer can check this value to set a cookie accordingly.

- Admin can configure cookie details and their description from the backend. The list of cookies can be displayed in your cookie policy page by using a shortcode.

- This plugin adds a subtle cookie banner to your website either in the header or footer so you can show your compliance status regarding the new GDPR law.

- You can fully customize the cookie notice style so it blends with your existing website.

- You can change the colors, fonts, styles, the position on the page and even how it behaves when you click “Accept”.

- Cookie Audit shortcode to construct a nicely-styled ‘Privacy & Cookie Policy’

Screenshots
===========

Cookie hint popup at Top left/right side OR Bottom left/right side corner of webpage.

.. figure:: Images/ext-cookies-hint-bottomright.jpeg
   :alt: Extension cookies hint bottom right/left side
   
Cookie hint popup at Top/Bottom side of the web page.

.. figure:: Images/ext-cookies-hint-top.jpeg
   :alt: Extension cookies hint top/bottom side
