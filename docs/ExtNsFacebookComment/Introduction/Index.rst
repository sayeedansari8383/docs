﻿
.. include:: ../Includes.txt

============
Introduction
============


ext:ns_facebook_comment
=======================

.. figure:: Images/ext-facebook-comment-banner.jpg
   :alt: Extension Banner 


What does it do?
================

Facebook comments plugin is a great tool that will allow you to show your visitors Facebook comments on your website’s particular page. At the same time this plugin is very useful for improving your website traffic from Facebook.

This plugin is easy to use, you just need to create Fb App ID and use it on your website.

**Key features of ns_facebook_comment extension:**

- Easy Moderation.

- Moderation with your Facebook Profile.

- Moderation with Facebook App.

- Facebook Comments Widget.

- Comments by Shortcode for pages and custom locations.

- Comments for all posts.

- Fully Responsive.

- Fully customizable.


Screenshot
===========

.. figure:: Images/facebook_comment.jpeg
   :alt: Snow Plugin 

