
.. include:: ../Includes.txt

============
Introduction
============


[NITSAN] News Slick Slider
==========================

.. figure:: Images/typo3-ext-news-slick-slider.jpg
   :alt: Extension Banner 


What does it do?
================

Add functionality to your TYPO3 website to display slick image slider to your news detail page using our TYPO3 plugin ns_news_slick.
Fully responsive, Swipe enabled, Desktop mouse dragging and Infinite looping.
Fully accessible with Autoplay, dots, arrows and much more.
It faciltates various slider options such as single image, multiple and responsive image view slider.
NS_News_Slick is lightweight, extensive, extendable, easy to use and be predestined to use with TYPO3.

**FEATURES:**

- 3 Image Slider view type Designs.

- Fully Responsive.

- Display unlimited number of slider images.

- Touch-enabled Navigation.

- Can be integrated to any news detail page.

- Fully accessible with arrow key navigation

- You can customize the extension according to your website needs like, give variable width, set image height/width, set transmission speed etc.

- Enable/Disable whether to display dots, auto-play, loop, center mode, etc.

- Define number of slide to display, how many slider image to scroll.

- You may include jQuery library also.

Screenshots
===========

.. figure:: Images/Preview_NS_News_Slick_slider.jpeg
   :alt: Extension Banner