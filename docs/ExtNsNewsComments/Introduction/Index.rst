﻿
.. include:: ../Includes.txt

============
Introduction
============


[NITSAN] Comment Plugin for EXT:news
====================================

.. figure:: Images/extension_banner.jpg
   :alt: Extension Banner 


What does it do?
================

It is an extension that facilitates visitor to post comment on a specific news, answer to the comments. Comments are the ideal way to deal and stay in touch with your visitors and adherences. This extension is compatible only with News system Extension (EXT:news).

**FEATURES:**

- Threaded Comments (with Nested-level Reply-to features)

- Responsive Design (Compatible with Cross-devices & Cross-browsers)

- Best User-Experiance with AJAX (without Re-load page)

- Comment Moderation Feature for Admin

- Enable/Disable Comments for fe_users (Frontend Loggedin Users)

- Get Email Notification to Admin on New Comments

- Set User (Commentator) Profile Image

- Restrict/Allow Annonymous Users to Comment

- Direct Comment Approval from Email sent to Admin

- Enable/Disable Google Captcha

- Comment Until Date

- Enable/Disable Captcha

- Custom Date & Time format

- Add "Terms" checkbox in Comment form

- Add your custom CSS/JS

- Each news have its own set comments

- Easy manipulation of comemnts

Screenshot
==========

Comment Form
^^^^^^^^^^^^

.. figure:: Images/comment_form.png
   :alt: Add Comment form 

Comment List
^^^^^^^^^^^^

.. figure:: Images/comments_list.png
   :alt: List of comments 