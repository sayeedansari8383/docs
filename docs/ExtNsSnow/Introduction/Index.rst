﻿
.. include:: ../Includes.txt

============
Introduction
============


[NITSAN] Snow Fall Plugin
=========================

.. figure:: Images/ext-snow-banner.jpg
   :alt: Extension Banner 


**This Christmas, Let it snow with our extension ns_snow!**

Christmas time? Do you want to add traditional snowfall to your TYPO3 site? Just use this plug-n-play TYPO3 extension with easy to use backend configuration.

What does it do?
================

Spread the cheers of winter by adding beautiful snowflakes and snowfall to your website.

Christmas Snow spreads the cheer of Christmas by adding beautiful snowflakes and snow-fall animation to your TYPO3 website with our little addon ns_snow and all the pages will show falling flakes of snow!

To make up for the lack of snow in our virtual world, you should see the snowflakes slowly zigging and zagging as they fall from the virtual sky!

**Key features of ns_snow extension:**

- Easy for admin to manage.

- Easy to enable or disable this extension from admin panel.

- No database and file override is required for this extension.

- Free extension.

- Friendly and flexible configuration.

- Cross-browsers compatibility.

- No conflict with other extensions.

- Make website Christmas ready!


Screenshot
===========

.. figure:: Images/snow.png
   :alt: Snow Plugin 

