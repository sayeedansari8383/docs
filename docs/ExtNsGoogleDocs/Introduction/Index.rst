﻿
.. include:: ../Includes.txt

============
Introduction
============


[NITSAN] Google Docs
=====================

.. figure:: Images/ext_banner.jpg
   :alt: Extension Banner 


What does it do?
================

EXT:ns_googledocs is the only TYPO3 extension which allows Backend user to import Google Docs to TYPO3 and convert the Google Docs content into TYPO3 Content elements.  


**FEATURES:**




Screenshots
===========

Backend Screenshots
^^^^^^^^^^^^^^^^^^^

**Dashboard**

.. figure:: Images/dashboard_updated.jpeg
   :alt: Dashboard tab 

**Import Google Docs**

.. figure:: Images/import_google_docs.PNG
   :alt: Import Google Docs 

**Reports & Logs**

.. figure:: Images/reports_and_logs.png
   :alt: Reports & Logs tab 

**Global Settings**

.. figure:: Images/global_settings.jpeg
   :alt: Global Settings Tab 