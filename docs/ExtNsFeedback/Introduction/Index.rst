﻿
.. include:: ../Includes.txt

============
Introduction
============


[NITSAN] Feedback
=====================

.. figure:: Images/ext_ns_feedback_introduction.jpg
   :alt: Extension banner 

What does it do?
================

EXT:ns_feedback is a TYPO3 extension which allows website Administartors to add Feedback forms to the site and get valuable feedbacks or Insights from visitors. EXT:ns_feedback provides variety of feedback forms from which administrator can choose depending on the website page.

Following Feedback forms are available with this extension:

- **1. Ratings feedback Form**
- **2. Quick Feedback Form**
- **3. Full Feedback Form**
- **4. Pop-up Feedback Form**


**FEATURES:**

- Dashboard
- Quick Feedback
- Rating Feedback
- Full Feedback Form
- Popup Feedback Form
- Feedback Form Settings
- Best User Experince AJAX Based-Features
- Reports Overview Module
- Report Detail
- Appearance, Design & Basic Settings
- Email Notification and Message Settings
- Ratings style, change etc
- Full backend settings
- Popup form configuration
- Integration to News extension as Plugin
- Integration to Blog extension as Plugin
- Multiple Feedback Forms in a single page


Screenshots
===========

Ratings Feedback Form
^^^^^^^^^^^^^^^^^^^^^

.. figure:: Images/rating_feedback_form.jpeg
   :alt: Ratings Feedback Form 

Quick Feedback Form
^^^^^^^^^^^^^^^^^^^^^

.. figure:: Images/quick_feedback_form.jpeg
   :alt: Quick Feedback Form 

Full Feedback Form
^^^^^^^^^^^^^^^^^^^^^

.. figure:: Images/full_feedback_form.jpeg
   :alt: Full Feedback Form 

Pop-up Feedback Form
^^^^^^^^^^^^^^^^^^^^^

.. figure:: Images/popup_feedback_form.jpeg
   :alt: Pop-up Feedback Form