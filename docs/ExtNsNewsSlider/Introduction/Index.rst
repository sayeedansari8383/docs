﻿
.. include:: ../Includes.txt

============
Introduction
============


ext:ns_news_slider
===================

.. figure:: Images/news_slider.jpg
   :alt: Extension Banner 


What does it do?
================

[NITSAN] News Slider Plugin allows you to add, manage and display news, widget, and vertical news scrolling widget on your website. This extension can be used to show the latest news on your website. ns_news_slider is one of the ways to effectively increase the dynamics of the online web space with news archives, scrolling news and thumbnails. Add, manage and remove the news section on your CMS website.


**Features**

- 4 designs for news layout: Nivo Slider, Owlcarousel Slider, Royal Slider, Slick Slider

- Fully responsive 

- Slider auto-play and speed interval

- Limit to display number of news

- Display news category wise

- Custom read more link for news

- Post filtration 

- Multilingual

- Asc or desc news order


Screenshots
===========

**Royal Slider**

.. figure:: Images/royal_slider.png
   :alt: Royal Slider Screenshot 


**Nivo Slider**

.. figure:: Images/nivo_slider.png
   :alt: Nivo Slider Screenshot 


**Owlcarousel Slider**

.. figure:: Images/owlcarousel_slider.png
   :alt: Owlcarousel Slider Screenshot 

**Slick Slider**

.. figure:: Images/slick_slider.png
   :alt: Slick Slider Screenshot 
