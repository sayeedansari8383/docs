.. include:: ../Includes.txt


News Slider Plugin
==================

=========================
1. Add News Slider Plugin
=========================

.. figure:: Images/add_news_slider.jpeg
   :alt: Add News Slider plugin

==============================
2. Select jQuery Slider Plugin
==============================

Here you need to select the jquery Plugin for News Slider. Also configure settings related to News (like Storage folder, categories, etc.) in Settings, Additional and Template tabs. 

.. figure:: Images/select_slider_type.png
   :alt: Select News Slider 