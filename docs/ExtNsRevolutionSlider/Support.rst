.. include:: Includes.txt

Support
-------

#. **Submit A Ticket** https://t3terminal.com/support-submit-ticket

#. **Email:** contact@t3terminal.com

#. **Web:** https://t3terminal.com
