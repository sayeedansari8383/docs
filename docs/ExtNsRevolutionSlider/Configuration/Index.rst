
.. include:: ../Includes.txt

.. _configuration:

=============
Configuration
=============

Quick & Easy Configuration of "Revolution Slider" into TYPO3
============================================================

.. rst-class:: ol-bignums

#. Switch to the *Template module* and select *Constant Editor*

#. Global Configuration: Select Category as follows *PLUGIN.TX_NSREVOLUTIONSLIDER_SLIDER (13)* You can configure it as per your requirement.

   .. figure:: Images/TYPO3-Revolution-Slider-Global-Configuration.png
      :alt: Configuration 1
      :width: 1000px

#. *Include Jquery* if you have not included yet in your project.

   .. figure:: Images/TYPO3-Revolution-Slider-Include-jQuery.png
      :alt: Configuration 3
      :width: 1000px

#. Create *Storage Folder* and Add "Slider" and "Slider Item" Records.

   .. figure:: Images/TYPO3-Revolution-Slider-Gallery-Records.png
      :alt: Configuration 4
      :width: 1000px

#. Now, Just Insert "NS Revolution Slider" Plugin, Choose your Storage Folder & Configure Slider Options.

   .. figure:: Images/TYPO3-Revolution-Slider-Add-Plugin.png
      :alt: Configuration 5
      :width: 1000px


You can configure your Revolution Slider with following settings:

**Main Settings tab**

- Slide Duration
- Slide Effect
- Headline Animation
- Description Animation
- Button Animation

**General Settings tab**

- Slider layout
- Slide Shadow (1-6 drop-shadow options)
- Spinner
- Stop loop
- Stop at slide
- Responsive levels
- Visibility levels
- Grid width
- Grid height
- Hide Slider At Limit (Hide the entire slider below a certain screen size. e.g. 414)
- Debug mode

**Navigation setting tab**

- Keyboard navigation enabled
- Keyboard direction
- Mouse scroll navigation enabled
- onHover stop
- Touch enabled
- Arrows enabled
- Arrows style
- Arrows Hide On leave
- Bullets enabled
- Bullets Style

**Font Style Settings tab**

- Headline Font Color
- Description Font Color
- Button Text Font Color
- Headline Font Size (Desktop view)
- Headline Font Size (Tablet view)
- Headline Font Size (Mobile view)
- Description Font Size (Desktop view)
- Description Font Size (Tablet view)
- Description Font Size (Mobile view)
- Button Text Font Size (Desktop view)
- Button Text Font Size (Tablet view)
- Button Text Font Size (Mobile view)


.. _Clearing-the-cache:

Clearing the cache
==================

Please use the buttons 'Flush frontend caches' and 'Flush general caches'
from the top panel. The 'Clear cache' function of the install tool will also
work perfectly.
