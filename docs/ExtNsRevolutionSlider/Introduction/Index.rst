﻿
.. include:: ../Includes.txt

============
Introduction
============

EXT:ns_revolution_slider
========================

.. figure:: Images/TYPO3-Revolution-Slider-Banner.jpg
   :alt: TYPO3 EXT ns_revolution_slider Banner
   :width: 800px

.. _What-does-it-do:

What does it do?
================

World's most popular revolution slider is available in TYPO3 CMS too. Of course, you will need to buy license "Revolution jQuery Slider" from Themepunch https://revolution.themepunch.com/jquery/


.. _Some-impressions:

Some impressions
================

.. figure:: Images/TYPO3-Revolution-Slider-Example.png
   :alt: TYPO3 EXT ns_revolution_slider Example
   :width: 800px