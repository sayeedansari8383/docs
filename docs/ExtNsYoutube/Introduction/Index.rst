﻿
.. include:: ../Includes.txt

============
Introduction
============

[NITSAN] Plugin for YouTube.com
===============================

.. figure:: Images/TYPO3_NS_YOUTUBE_NITSAN_Banner_Preview.jpg
   :alt: TYPO3 EXT TYPO3_API_StatCounter_NITSAN_Banner_Preview Banner
   :width: 1200px

.. _What-does-it-do:

What does it do?
================

One of the only TYPO3 extension which provides to integrate all the features of Youtube.com as follows:

- Simple video integration
- Show Playlist
- Show Channel
- Gallery View
- Live-stream videos
- Flexibile backend configuration
- Industrial Technical Standards (Extbase/Fluid, Hooks etc.,)
- Compatible from TYPO3 6.x to 9.x

.. _Screen-shots:

Screen Shots
================

**1. Display Playlist as Gallery**
-------------------------

.. figure:: Images/playlist_gallery.jpeg
   :alt: screenshot "TYPO3_NS_YOUTUBE_Screenshot_1"
   :class: with-shadow
   :width: 1100px


**2. Display Complete Playlist**
---------------------

.. figure:: Images/complete_playlist.jpeg
   :alt: screenshot "TYPO3_NS_YOUTUBE_Screenshot_2"
   :class: with-shadow
   :width: 1100px


**3. Embed a YouTube Channel (User/Channel ID)**
---------------------

.. figure:: Images/youtube_channel.jpeg
   :alt: screenshot "TYPO3_NS_YOUTUBE_Screenshot_3"
   :class: with-shadow
   :width: 1100px


**4. Display Single Video**
--------------------

.. figure:: Images/single_video.jpeg
   :alt: screenshot "TYPO3_NS_YOUTUBE_Screenshot_4"
   :class: with-shadow
   :width: 1100px