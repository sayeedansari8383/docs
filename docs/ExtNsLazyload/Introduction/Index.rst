﻿
.. include:: ../Includes.txt

============
Introduction
============

[NITSAN] Lazy Load
==================

.. figure:: Images/lazy-load.jpg
   :alt: Extension Banner 

What does it do?
================

Are you facing speed and performance issue due to assets on your TYPO3 site? If yes, This plug-an-play extension reduces the number of HTTP requests mechanism and improves the loading time.


Screenshot
==========

.. figure:: Images/custom_element.png
   :alt: Custom element
