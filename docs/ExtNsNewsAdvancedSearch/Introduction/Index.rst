
.. include:: ../Includes.txt

============
Introduction
============


[NITSAN] News Advance Search
============================

.. figure:: Images/typo3-ext-advance-search-for-ext-news.jpg
   :alt: Extension Banner 


What does it do?
================

This plugin extends your search results and allows to search by content with filters in fields created using Advanced plugin. This plugin use specially designed Search algorithms for that purpose, which gives you both speed and relevance. 
Everything is fully configurable in the TYPO3 settings for the plugin. Also available for TYPO3 MU (multisites).

**FEATURES:**

- Choice search columns

- Using a relevance algorithm for the final classification

- Detailed search from specific fields

- Several system to display results (pagination, trigger or infinite scroll)

- Highlight search terms, or not

- Ability to write an exact search with words in quotation marks

- Opportunity to show the results of specific categories


Screenshots
===========

.. figure:: Images/FE-ext-advance-search-for-ext-news.jpeg
   :alt: Extension Banner
