﻿
.. include:: ../Includes.txt

============
Introduction
============


[NITSAN] Maintenance Mode
=========================

.. .. figure:: Images/ext_banner.jpg
   :alt: Extension Banner 

What does it do?
================

Do you want to make your site temporary offline like coming soon and maintenance mode? This extension will help you with simple plug & play to your TYPO3 instance. 

**FEATURES:**

- Enable/Disable Maintenance Mode

- Show Countdown with different styles

- Set Theme Layouts

- Whitelist IP Addresses

- Configure Image in Background

- Configure Solid Background Color

- Set Font Color of Text

- Set Title, Heading & Introduction Text

- Configure End Date/Expiry Date

- Set Footer Text

- Set Social platform links


Screenshots
===========

**Maintenance screen with Subscription box and Background Image**

.. figure:: Images/Maintenance_subscription.png
   :alt: Maintenance Subscription 

**Maintenance screen with countdown in Square box**

.. figure:: Images/Maintenance_squarebox.png
   :alt: Maintenance Squarebox 

**Maintenance screen with countdown in Circles**

.. figure:: Images/Maintenance_circle.png
   :alt: Maintenance Circle 

