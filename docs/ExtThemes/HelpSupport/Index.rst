.. include:: ../Includes.txt

.. |true-icon| image:: Images/righ-icon.png
.. |false-icon| image:: Images/crose-icon.png
.. |view-icon| image:: Images/view.png
.. |version-icon| image:: Images/version.png
.. |documentation-icon| image:: Images/documentation.png
.. |typo-icon| image:: Images/typo.png


.. _Action_And_Results:

==============
Help & Support
==============

Support: Submit A Ticket
========================
https://t3terminal.com/support-submit-ticket

Email
=====
contact@t3terminal.com

Tel
===
+91 0278 2568181

Thank you very much!
====================

Have an awesome websites with our TYPO3 Templates!
