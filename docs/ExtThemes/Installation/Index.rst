.. include:: ../Includes.txt

.. _installation:

============
Installation
============

Install TYPO3 Template with Composer
====================================

We are supporting composer for our Free & Premium TYPO3 templates too.

1. For Free TYPO3 Templates:
You can find composer command at particular TYPO3 template product page at T3Terminal Marketplace https://t3terminal.com/typo3-templates

2. For Premium TYPO3 Templates:
Please create support ticket https://t3terminal.com/support-submit-ticket


Install TYPO3 Template without Composer
=======================================

T3Terminal's TYPO3 Templates are very easy to install and configure.

Step 1: Go to Extension Manager

Step 2: Install "ns_basetheme" Extension, Get latest version from here https://extensions.typo3.org/extension/ns_basetheme/

Step 3: (Optional) Install required dependendent TYPO3 extensions of your template eg., gridelements, You may get list of dependencies TYPO3 extensions from Template's detail page

Step 4: Install your purchased TYPO3 template zip eg., ns_theme_agency_1.2.0.zip

.. figure:: Images/install_basetheme.jpeg
   :alt: Install Base Template


.. Note:: If you are installing Template using Composer then you will have to uninstall and re-install TYPO3 Template EXT:ns_theme_name


Site Management
===============

Go to Site Management > Sites > Edit Record > You need to configure "Entry Point" by entering your website URL

.. figure:: Images/sitemanagement.jpeg
   :alt: Site Management
   :width: 1300


.. Note:: If you have composer based TYPO3 installation, then make create folder /config/ folder at your root, and copy/paste /public/typo3conf/ext/ns_basetheme/sites folder - to get default Site management configuration. 

Page Tree
=========

Once you install your TYPO3 Template extension, it will automatically generate "Page tree" in your TYPO3 backend with all the pages and content.

.. figure:: Images/pagetree.jpeg
   :alt: Page Tree

SEO Setup for TYPO3 9 and TYPO3 10
==================================

Once you have completed Site Management, you can setup SEO for the site. For this, you have to add XML Sitemap in Include Static in Template record. You can perform this as shown in below screenshot.

.. figure:: Images/how_to_include_xml_sitemap_ext.jpeg
   :alt: How to include XML Sitemap in Template?

To configure error Handling in Site and setup 404, you need to perform steps as mentioned below:

1. Go to Sites Module.
2. Select the Site
3. Switch to Error Handling tab and click on Create New button.
4. Set appropriate HTTP Error Status Code and set how to Handle Errors field.

.. figure:: Images/how_to_set_error_handling_in_site.jpeg
   :alt: How to set error handling in site?

