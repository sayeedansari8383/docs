.. include:: ../Includes.txt

.. _faq:

=============
Customization
=============

We have created good Templating Framework using most-famous parent and child theme concept. If you know little about TYPO3-integration, then to add new custom content element is very simple. Our templating structure is so each, You will need to write sinlge line of code for TypoScript or PHP, Just follow these simple steps.

Edit Content Elements
=====================

To change the HTML wrapper or style, You can edit existing content element's Fluid-templates with following steps.

Step 1. Go to typo3conf/ext/ns_theme_<themename>/Resources/Private/Components/

Step 2. Edit Fluid-Template which you wanted to Customize


Create Content Elements
=======================

Step 1. Create ns_<elementname>.xml at typo3conf/ext/ns_theme_<themename>/Configuration/FlexForms/ You need to configure FlexForm configuration to generate form, You can check existing reference within the same folder. 

.. figure:: Images/T3Terminal-TYPO3-Create-Flex.png
   :alt: T3Terminal-TYPO3-Create-Flex
   :width: 1300px

Step 2. Create Ns<ElementName>.html at typo3conf/ext/ns_theme_<themename>/Resources/Private/Components/ You just need to render element with TYPO3's core Fluid-Template.

.. figure:: Images/T3Terminal-TYPO3-Render-Fluid.png
   :alt: T3Terminal-TYPO3-Render-Fluid
   :width: 1300px


That's it, You can already see your new element into "Custom Elements" at add new content wizard.

.. figure:: Images/T3Terminal-TYPO3-Custom-Element.png
   :alt: T3Terminal-TYPO3-Custom-Element
   :width: 1300px


Set Backend Preview
===================

Wait.. Do you want nice backend preview? You can configure backend layout Fluid-Template
- Step 3. Create Ns<ElementName>.html at typo3conf/ext/ns_theme_<themename>/Resources/Private/Components/Backend/

.. figure:: Images/T3Terminal-TYPO3-Backend-Preview-Code.png
   :alt: T3Terminal-TYPO3-Backend-Preview-Code
   :width: 1300px

.. figure:: Images/T3Terminal-TYPO3-Backend-Preview.png
   :alt: T3Terminal-TYPO3-Backend-Preview
   :width: 1300px