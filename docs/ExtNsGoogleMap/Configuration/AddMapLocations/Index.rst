.. include:: ../Includes.txt



===========================
Add Google Map Locations
===========================

First of all, you need to create Locations to display on Map.

You can do it by performing following steps:

Step 1: Create a Storage Folder for Map Locations.

Step 2: Create Map Location in above storage folder.

.. figure:: Images/add_location.jpeg
   :alt: Add Google Locations record

Now you can fill all the details of location.

|

.. figure:: Images/add_location_1.jpeg
   :alt: Add Google Locations (General tab)

**Title** : Set title of the location.

**Map**: Set Location Name and click on update button. If found, location will be highlighted on map below it.

**Image for Marker**: You can set marker image for this location.

**Lat/Long & Address**: Latitude, Longitude and Address textboxes will be auto-generated with location selected above.

|
|

.. figure:: Images/add_location_2.jpeg
   :alt: Add Google Locations (Info Content tab)

**Infocontent**: Set the address you want to display with Location title in plugin

**View Goole Map Link**: Check this if you want to display Google Map link along with Location details.

**Interaction**: Set how location popup will behave in Plugin.

This way, create all the locations you want to display on Map.