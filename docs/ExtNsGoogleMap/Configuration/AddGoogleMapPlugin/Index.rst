.. include:: ../Includes.txt


========================
Add Google Map Plugin
========================

Add Google Map Plugin from Add New Element popup.

.. figure:: Images/add_google_map.jpeg
   :alt: Add Google Map Plugin



2.1 General Settings
=====================

.. figure:: Images/general_settings_updated.jpeg
   :alt: General Settings of Map Plugin

- **Address** -> Select the locations you want to display on the Map with Location marker.

- **Width** -> Set the width of Map

- **Height** -> Set the height of Map

- **Enable Radius Search Feature** -> Check this to enable radius search box to appear above Map. This feature will allow end user to search for places on Google Map by providing their preference of City, Point of Interest and radius.

- **Select Theme** -> Select appropriate theme for the map. You can select from following options: Default Theme, Express Route, Dark blue Power, Energy, Ultra Light, Green Serenity, Black And White

- **Enable Direction and Distance Feature** -> Check this to allow users to fetch routes b/w the places as per their choice of transportation mode. It'll also provide the directions for the selected route at the bottom of the Map.


2.2 Map Settings
================

.. figure:: Images/map_settings.jpeg
   :alt: Map Settings in Map Plugin

- **Map Type** -> Select the Map type you want to display from Map, Satellite, Hybrid and Terrain

- **Zoom Level** -> Select the default zoom level of map when map is displayed. You can select from range from 1 to 22 with 1 being completely Zoomed Out and 22 being completely Zoomed In.

- **Scroll Zoom** -> Check this to enable Zoom In and Zoom Out using Mouse scroll.

- **Draggable** -> Check this to allow users to drag map using mouse.

- **Doubleclick Zoom** -> Check this to enable Zoom In when user double-click on map.


2.3 Control Settings
=====================

.. figure:: Images/controls_settings.jpeg
   :alt: Controls Settings in Map Plugin

- **Map type Control** -> Check this to allow users to change Map type. Once checked, you can select which types of Map user can view. You can also set position on chart as well.

- **Zoom Control** -> Check this to display Zoom buttons on map. You can also set position of it on chart as well.

- **Fullscreen Control** -> Check this to display Full screen button on Map. You can also set position of it on chart as well.

- **Street View Control** -> Check this to display Streetview control on Map. You can also set position of it on chart as well.

- **Scale Control** -> Check this to display scale bar at the bottom right of Map. 



2.4 Marker Settings
====================

.. figure:: Images/marker_settings.jpeg
   :alt: Controls Settings in Map Plugin

- **Show alphabet label on marker** -> Check this to display alphabet labels on Location markers.

- **Marker Opacity** -> Set Marker Opacity. Select from 0.1 to 1.0 .

- **Marker Cluster** -> Check this to enable Marker Cluster.