﻿
.. include:: ../Includes.txt

============
Introduction
============


[NITSAN] Google Map
===================

.. figure:: Images/ext-google-map-banner.jpg
   :alt: Extension Banner 


What does it do?
================

The easiest to use Google maps plugin! Add a customized Google map to your TYPO3 website quickly and easily with the NS Google Map. Perfect for contact page maps, routes, maps, restaurants, hospitals areas and any other use you can think of!
Google Maps allows you to create a Google map with as many markers as you like.

**FEATURES:**

- Super easy to use, no coding required!

- Create as many map markers as you need by simply typing in the address

- Responsive maps 

- Edit your map markers with the click of a button

- 7 popular map themes to choose from

- Add a map block to your page

- UTF-8 character support

- Full screen map functionality

- Support for localization

- Store locator functionality

- Zoom level for your map

- Latest Google Maps API


Screenshots
===========

.. figure:: Images/google_map1.jpeg
   :alt: Google Map Plugin 

