﻿
.. include:: ../Includes.txt

============
Introduction
============


[NITSAN] Protect Site
=====================

.. figure:: Images/ext_protect_site_banner.jpg
   :alt: Extension Banner 


What does it do?
================

Authentication at TYPO3 - simple though sufficiently powerful feature that provides Administrators & TYPO3 back-end users a way to restrict accessibility of any page with only users having password can access page.

Each page of your website can have different password protection which makes it very much protected from unauthenticated access. Also, page to enter password is very Clean, specific and user-friendly. You can setup it as per your wish.

**FEATURES:**

- Easy setup

- Can set unique password for each page

- Make page accessible to authenticated users

- Set Image, your logo, text & Validation message as per your wish

- Easy to enable/disable protection

- Default TYPO3 password algorithm


Screenshot
==========

.. figure:: Images/protected_page.jpeg
   :alt: Protected Page 

