﻿
.. include:: ../Includes.txt

============
Introduction
============


[NITSAN] Plugin for Zoho API Integration
========================================

.. figure:: Images/typo3-ext-zoho.jpg
   :alt: Extension Banner 


'**[NITSAN] Plugin for Zoho API Integration**' For ZOHO CRM enables to capture data from your contact forms to your CRM as Leads or Contacts. You can push or convert data from default form or contact form embedded into your website as leads.


**Features:**

- Generate forms with or without 3rd party web form.

- Embed forms in Page, Post or extension to capture CRM Lead.

- Captures potential leads to Zoho CRM and Zoho CRM Plus

- Converts new Users as your CRM Contact.

