﻿
.. include:: ../Includes.txt

============
Introduction
============


[NITSAN] Cookiebot.com TYPO3 GDPR/EPR Compliant
===============================================

.. figure:: Images/ext-banner.jpg
   :alt: Extension Banner 


What it is?
===========
To support publishers, technology vendors and advertisers in meeting the transparency and user consent requirements of the GDPR and ePrivacy Directive, NITSAN has designed an extension NS_Cookiebot in TYPO3 CMS to integrate one of the most popular cookiebot.com

**Key features of ns_cookiebot extension:**

- A highly customizable consent banner to handle user consents and give the users the required possibility to opt-in and -out of cookie categories.

- A cookie policy and declaration, with purpose descriptions and automatic categorization of your cookies (strictly necessary, preference, statistics, marketing).

- Full monthly scans to detect all tracking in place on the website as well as detection of where data is being sent to and where in the source code the cookie can be found.

- A scanner that detects various online trackers such as Cookies, HTML5 Local Storage, Flash Local Shared Object, Silverlight Isolated Storage, IndexedDB, ultrasound beacons, pixel tags etc.

- An easy way to allow the users to change or withdraw their consent.

- Translations for 44 languages and the ability to change the text on the banner and declaration for any language.

- Storage of user-consents in our cloud-driven environment, which are downloadable and can be used as proof.

- Execution of cookie-setting scripts without a page reload, if the user gives consent.

What does it do?
================

NS_CookieBot facilitates the website holder by displaying a cookie statement bar at every page of the website till the visitor accepts it.

Installing CookieBot on a TYPO3 site is now made very easy. You do not need to change the source code of your template, cookie statements can be quickly and easily placed on your site.


Screenshot
===========

.. figure:: Images/Cookiebot.jpg
   :alt: Cookiebot 

