﻿
.. include:: ../Includes.txt

============
Introduction
============


[NITSAN] Guestbook TYPO3 Plugin
===============================

.. figure:: Images/ext-banner.png
   :alt: Extension Banner 

NS Guestbook extension is an open source TYPO3 guest book plugin with spam protection and many customization options. NS Guestbook gives functionality for your guests to share their reviews, ratings, contact details or personal details with experience sharing and much more.

Guest user adds their details and can see all guest user’s guest book detail. Admin can see stories which are submitted by users in Guest book tab. Admin can edit, delete and published/unpublished guest entry. The feature of guest entry auto publish is configurable from the backend.

**Key features of ns_guestbook extension:**

- Easy to install and easy to configure.

- Spam protection with captcha.

- Admin easy to manage the guest entry from the back-end.

- Responsive Design

- Ajax submit form

- Email notification

- Meta Fields

- Preview for the frontend form

- Preview for the admin editor form

Screenshots
===========

**Guestbook Form**

.. figure:: Images/guestbook_form.jpeg
   :alt: Guestbook Form 

**Message list from Guests**

.. figure:: Images/guestbook_messages.jpeg
   :alt: Message list from Guests 
