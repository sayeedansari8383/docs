.. include:: ../Includes.txt

======================
Start One-Click Backup
======================

You can easily take quick manual backup with following steps.

**Step 1.** Go to Admin Tools > NS Backup

**Step 2.** Click on "Start One-Click Backup" button

**Step 3.** Enter your backup name

**Step 4.** Choose what do you want to backup, like Backup-Everything, Database etc.

**Step 5.** Select your configured server/cloud and click-on "Start Backup Now!" button.

.. Tip:: Based on size of your website's database, code and assets, It may take more time to take backup. If you have bigger size website, then We recommend to create Scheduler and take backup from System > TYPO3 Scheduler. Checkout https://docs.t3terminal.com/en/latest/ExtNsBackup/ScheduleBackup/Index.html#run-backup-from-typo3-core-scheduler

.. Attention:: In this extension, we are executing .phar file with PHP's exec() and shell_exe(). Many server have restriction to execute such server-level stuff. So, If "Start One-Click Backup" does not work for you, then you should only use TYPO3-CLI feature Checkout https://docs.t3terminal.com/en/latest/ExtNsBackup/ScheduleBackup/Index.html#run-backup-from-typo3-cli

.. figure:: Images/ns-backend-start-manual-backup.png
   :alt: ns-backend-start-manual-backup

.. figure:: Images/ns-backup-typo3-start-one-click-backup-success.png
   :alt: ns-backup-typo3-start-one-click-backup-success