.. include:: ../Includes.txt

=======
Support
=======

.. Note::
    - Submit A Ticket: https://t3terminal.com/support-submit-ticket
    - Contact: contact@t3terminal.com
    - Web: https://t3terminal.com/