﻿
.. include:: ../Includes.txt

============
Introduction
============


[NITSAN] Gallery
================

.. figure:: Images/ext_banner.jpg
   :alt: Extension banner 

What does it do?
================

EXT:ns_gallery is a TYPO3 extension which allows website Administartors to add beautiful Galleries to their website. EXT:ns_gallery provides variety of galleries to choose from and allow administrators to present media to website user in best possible way. Galleries in this extensions are not limited to just images and it also allow to add self-hotsted Videos, YouTube Videos & Vimeo Videos.

Following Galleries are available with this extension:

**1. Album View (Open in same/new page)**

**2. Masonary View**

**3. Isotope View**

**4. Mosaic View**

**5. Slider View & Carousel View**

**6. Google Search View**

**7. Zoom View**


**FEATURES:**

- Backend Dashboard
- Manage Gallery & Albums from Backend
- Load More / Pagination
- Responive Galleries
- DeepLinking/Hash
- Lazy Load
- Themes (Default, Dark)


**Lightbox FEATURES:**

- Navigation arrows
- Full Screen
- Mouse Dragging
- Image Counter
- Slider Loop
- Thumbnail
- Zoom option
- Slideshow
- Manage Slideshow Speed
- Download images

Screenshots
===========

Album View
^^^^^^^^^^

.. figure:: Images/album_view.jpeg
   :alt: Album Gallery 

Masonary View
^^^^^^^^^^^^^

.. figure:: Images/masonary_view.jpeg
   :alt: Masonary Gallery 

Isotope View
^^^^^^^^^^^^

.. figure:: Images/isotope_view.jpeg
   :alt: Isotope Gallery 

Mosaic View
^^^^^^^^^^^

.. figure:: Images/mosaic_view.jpeg
   :alt: Mosaic Gallery

Slider View
^^^^^^^^^^^

.. figure:: Images/slider_view.jpeg
   :alt: Slider Gallery

Carousel View
^^^^^^^^^^^^^

.. figure:: Images/carousel_view.jpeg
   :alt: Carousel Gallery

Google Search View
^^^^^^^^^^^^^^^^^^

.. figure:: Images/google_search_view.jpeg
   :alt: Google Search View

Zoom View
^^^^^^^^^

.. figure:: Images/zoom_view.png
   :alt: Zoom View

Lightbox View
^^^^^^^^^^^^^

.. figure:: Images/lightbox_preview.jpeg
   :alt: Lightbox View
