
.. include:: ../Includes.txt

.. _configuration:

=============
Configuration
=============

Quick & Easy configuration of "All Sliders" into TYPO3
======================================================

.. rst-class:: ol-bignums

#. Switch to the *Template module* and select *Constant Editor*

#. Select Category as follows *PLUGIN.TX_NSALLSLIDERS_OWLCAROUSEL (17), PLUGIN.TX_NSALLSLIDERS_ROYALSLIDER (29),  
   PLUGIN.TX_NSALLSLIDERS_SLIDERJS (14), PLUGIN.TX_NSALLSLIDERS_NIVOSLIDER (4)* You can configure it as per your requirement.

   .. figure:: Images/select_slider.jpeg
      :alt: Configuration 1
      :width: 1000px

#. *Include Jquery* if you have not included yet in your project.

   .. figure:: Images/include_jquery.jpeg
      :alt: Configuration 2
      :width: 1000px

#. Create *Storage Folder* for this plugin.

   .. figure:: Images/create_gallery.jpeg
      :alt: Configuration 4
      :width: 1000px

#. Add those plugins in to page where you want to use these sliders. And configure it as per your requirement. Also add *Storage Folder* where your slider images are stored.

   .. figure:: Images/setup_plugin.jpeg
      :alt: Configuration 5
      :width: 1000px

.. _Clearing-the-cache:

Clearing the cache
==================

Please use the buttons 'Flush frontend caches' and 'Flush general caches'
from the top panel. The 'Clear cache' function of the install tool will also
work perfectly.
