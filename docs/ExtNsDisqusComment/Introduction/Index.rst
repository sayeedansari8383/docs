﻿
.. include:: ../Includes.txt

============
Introduction
============


[NITSAN] Disqus.com Plugin
==========================

.. figure:: Images/ext-banner.jpg
   :alt: Extension Banner 


What does it do?
================

DISQUS is a global comment system that improves discussion on websites and has many other features.

The EXT:ns_disqus_comments extension will help you to integrate DISQUS comments plugin into your website. Our extension will integrate DISQUS comments section on TYPO3 pages (you can easily enable or disable comments block on your pages).

DISQUS comments section allows users to comment your pages by using their favorite Social Network (Facebook, Twitter or Google), easy get notifications about new answers, share messages and a lot of more.

**Key features of ns_disqus_comments extension:**

- Simple one-click installation that seamlessly integrates with TYPO3 without ever needing to edit a single line of code or losing any of your existing comments

- Keep users engaged on your site longer with a commenting experience that readers love

- Bring users back to your site with web and email notifications and personalized digests

- Comment text formatting (e.g. bold, link, italics, quote)

- Threaded comment display with nesting and ability to collapse individual threads

- Sort discussion by oldest, newest, and best comments

- Flexible login options – Social login with Facebook, Twitter, and Google, SSO, and guest commenting support

Screenshot
===========

.. figure:: Images/disqus_comments.jpeg
   :alt: Disqus Comments Plugin 

